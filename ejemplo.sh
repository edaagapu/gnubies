#!/bin/bash

#Recibir una entrada para imprimir en consola
#Ejecución de programa en Python 3

python3 programa.py < archivo.txt

#Enviar la salida de un comando/programa a un archivo de texto

ls > archivo.txt
ls 1> archivo.txt

#Enviar la salida de un comando/programa a un archivo de texto (Sin eliminar su contenido anterior)

ls >> archivo.txt

#Enviar la salida de error a un archivo de texto

ls 2> archivo.txt

#Encadenar dos comandos con tuberías
#Mirar procesos que se estan ejecutando, y organizarlos de forma alfabetica

ps -a | sort

#¿Se puede encadenar una redirección de entrada y una de salida? SI.

python3 programa.py < archivo.txt 1> respuesta.txt

#Imprimir salida estándar y salida de error en un mismo archivo

python3 programa.py 1&2> salida.txt

#Ejecución de programas en C++
#Compilar
g++ -o launcher programa.cpp
#Ejecutar
./launcher


